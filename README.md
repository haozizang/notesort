# noteSorter
* this project is planned to make a script to sort notes.


# unbind-key -a

### prefix reset
# set -g prefix C-a
# unbind C-b
# bind C-a send-prefix

### mouse options
# set-option -g mouse on

# switch pane remap
bind -r k select-pane -U
bind -r j select-pane -D
bind -r h select-pane -L
bind -r l select-pane -R

# resize remap
bind -r ^k resizep -U 10
bind -r ^j resizep -D 10
# in tmux for some tty C-H -> ^? instead ^H; backspace -> ^H
bind -r BSpace resizep -L 10
bind -r ^l resizep -R 10
# bind -r Up resizep -U 10
# bind -r Down resizep -D 10
# bind -r Left resizep -L 10
# bind -r Right resizep -R 10


# copy
setw -g mode-keys vi
bind Escape copy-mode 
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-selection-and-cancel
bind C-v pasteb


# tmux-resurrect
run-shell ~/.tmux/plugins/tmux-resurrect/resurrect.tmux




" <=====code format=====
filetype indent on
set cindent
set cinoptions=g0,:0,N-s,(0
set backspace=2
syntax on
set tabstop=4
set softtabstop=4
set shiftwidth=4
" <====convert tab -> 4 spaces
set expandtab
" convert tab -> 4 spaces===>
set fileencoding=utf-8
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
set laststatus=2
set autoindent
" =====code format=====>

" <=====line number=====
set number relativenumber
" set nu rnu

" <=====theme=====   
colorscheme delek
" =====theme=====>


" <======others=====
set cursorline
nnoremap <leader>h :set cursorline<CR>
hi CursorLine term=bold cterm=bold guibg=Grey40
nnoremap U :noh<CR>
nnoremap <C-]> g<C-]>
set nobackup

" undo after closing file
if has('persistent_undo')
    set undofile
    set undodir=~/.vim/undo
endif

set undolevels=1000
set undoreload=10000
" =====others=====>


