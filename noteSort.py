import os

for file in os.listdir('./'):
    if file.endswith(".txt"):
        with open(file, "r", encoding='utf-8') as outfile:
            lines = outfile.readlines()
            lines.append("\n")
            lines.append("$$$")

        # use a mark to skip files that have been sorted
        if lines[-3].startswith("$$SORTED"):
            continue
        # initially set flag as False
        take_flag=False
        note_content = ""
        note_category = ""
        # print(lines)
        for line in lines:
            if line.startswith("$$$"):
                if note_category:
                    with open("../"+note_category, "a+", encoding='utf-8')as infile:
                        infile.write(note_content)
                take_flag = True
                note_category = line[3:-1]
                note_content = ""
                continue
            elif line.startswith("@@@"):
                take_flag = False
                continue

            if take_flag:
                note_content += line

        with open(file, "a+", encoding='utf-8') as outfile2:
            outfile2.write("\n")
            outfile2.write("$$SORTED")
